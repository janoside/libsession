package com.janoside.session.service;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.cache.MemoryCache;
import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.pool.SimpleObjectFactory;
import com.janoside.session.Session;

public class SharedSessionServiceTest {
	
	@Test
	public void testGetSession() throws InterruptedException {
		SharedSessionService<Session> sss = this.getService(true);
		
		Session session = sss.getSession("abc");
		
		Assert.assertEquals("abc", session.getId());
		
		session = sss.getSession("abc");
		
		Assert.assertEquals("abc", session.getId());
		
		Session session2 = new Session();
		session2.setId("abc");
		session2.getData().put("one", "two");
		
		sss.updateSession("abc", session2);
		sss.updateSession("abc", session2);
		
		session = sss.getSession("abc");
		
		Assert.assertSame(session, session2);
		Assert.assertEquals("abc", session.getId());
		Assert.assertTrue(session.getData().has("one"));
		Assert.assertEquals("two", session.getData().getString("one"));
		
		sss.removeSession("abc");
		
		session = sss.getSession("abc");
		
		Assert.assertFalse(session == session2);
	}
	
	@Test
	public void testExpire() throws InterruptedException {
		SharedSessionService<Session> sss = this.getService(true);
		
		Session s1 = sss.getSession("def");
		
		Thread.sleep(300);
		
		Session s2 = sss.getSession("def");
		
		Assert.assertFalse(s1 == s2);
		
		s1 = sss.getSession("haha");
		Thread.sleep(100);
		s2 = sss.getSession("haha"); // auto-renew = true
		Assert.assertSame(s1, s2);
		Thread.sleep(200);
		Session s3 = sss.getSession("haha");
		Assert.assertSame(s1, s3);
		
		sss.setAutoRenew(false);
		Assert.assertFalse(sss.isAutoRenew());
		
		s1 = sss.getSession("haha");
		Thread.sleep(100);
		s2 = sss.getSession("haha"); // auto-renew = false
		Assert.assertSame(s1, s2);
		Thread.sleep(200);
		s3 = sss.getSession("haha");
		Assert.assertFalse(s1 == s3);
	}
	
	@Test
	public void testNoCache() throws InterruptedException {
		SharedSessionService<Session> sss = this.getService(false);
		
		Session s1 = sss.getSession("def");
		
		s1.getData().put("one", "two");
		
		sss.updateSession("def", s1);
		
		sss.setAutoRenew(false);
		
		sss.getSession("def");
		
		sss.setAutoRenew(true);
		
		sss.getSession("def");
	}
	
	@Test
	public void testInsaneCache() throws InterruptedException {
		SharedSessionService<Session> sss = this.getService(false);
		
		InsaneCache ic = new InsaneCache();
		sss.setCache(ic);
		sss.setExpiration(Integer.MAX_VALUE);
		
		sss.getSession("def");
		
		ic.putFailureCount = 1;
		
		sss.getSession("def");
	}
	
	private SharedSessionService<Session> getService(boolean withCache) {
		StandardErrorExceptionHandler exceptionHandler = new StandardErrorExceptionHandler();
		
		SharedSessionService<Session> sss = new SharedSessionService<Session>();
		sss.setSessionFactory(new TestSessionFactory());
		
		if (withCache) {
			sss.setCache(new MemoryCache<Session>());
		}
		
		sss.setExceptionHandler(exceptionHandler);
		sss.setExpiration(250);
		sss.setAutoRenew(true);
		
		Assert.assertEquals(250, sss.getExpiration());
		Assert.assertTrue(sss.isAutoRenew());
		
		return sss;
	}
	
	private static class TestSessionFactory implements SimpleObjectFactory<Session> {
		
		public Session createObject() {
			return new Session();
		}
	}
	
	private static class InsaneCache extends MemoryCache<Session> {
		
		public int putFailureCount;
		
		public int getFailureCount;
		
		public int getFailureSkip;
		
		public void put(String key, Session value, long lifetime) {
			if (putFailureCount == 0) {
				super.put(key, value, lifetime);
				
			} else {
				putFailureCount--;
				
				throw new RuntimeException();
			}
		}
		
		public Session get(String key) {
			if (getFailureSkip == 0) {
				return super.get(key);
				
			} else {
				getFailureSkip--;
				
				if (getFailureCount == 0) {
					return super.get(key);
					
				} else {
					getFailureCount--;
					
					throw new RuntimeException();
				}
			}
		}
	}
}