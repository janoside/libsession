package com.janoside.session;

import java.io.Serializable;

import com.janoside.json.JsonObject;

@SuppressWarnings("serial")
public class Session implements Serializable {
	
	protected JsonObject data;
	
	protected String id;
	
	private int hash;
	
	public Session() {
		this.data = new JsonObject();
	}
	
	public JsonObject getData() {
		return this.data;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public int getHash() {
		return this.hash;
	}
	
	public void computeHash() {
		this.hash = this.hashCode();
	}
	
	public String toString() {
		return "Session(id=" + this.id + ", data=" + this.data.toString() + ")";
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
}