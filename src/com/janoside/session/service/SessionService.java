package com.janoside.session.service;

import com.janoside.session.Session;

public interface SessionService<T extends Session> {
	
	T getSession(String id);
	
	void updateSession(String id, T session);
	
	void removeSession(String id);
}