package com.janoside.session.service;

import com.janoside.cache.ObjectCache;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.pool.SimpleObjectFactory;
import com.janoside.session.Session;
import com.janoside.util.DateUtil;

public class SharedSessionService<T extends Session> implements SessionService<T>, ExceptionHandlerAware {
	
	private ObjectCache<T> cache;
	
	private SimpleObjectFactory<T> sessionFactory;
	
	private ExceptionHandler exceptionHandler;
	
	private long expiration;
	
	private boolean autoRenew;
	
	public SharedSessionService() {
		this.expiration = 30 * DateUtil.MinuteMillis;
		this.autoRenew = true;
	}
	
	public T getSession(String id) {
		T session = null;
		
		try {
			session = this.cache.get(id);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
		
		if (session == null) {
			session = this.sessionFactory.createObject();
			session.setId(id);
			
			session.computeHash();
			
			try {
				this.cache.put(id, session, this.expiration);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		} else if (this.autoRenew) {
			try {
				this.cache.put(id, session, this.expiration);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
		
		return session;
	}
	
	public void updateSession(String id, T session) {
		int previousHash = session.getHash();
		
		session.computeHash();
		
		if (session.getHash() != previousHash) {
			try {
				this.cache.put(id, session, this.expiration);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	public void removeSession(String id) {
		this.cache.remove(id);
	}
	
	public void setCache(ObjectCache<T> cache) {
		this.cache = cache;
	}
	
	public void setSessionFactory(SimpleObjectFactory<T> sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public long getExpiration() {
		return this.expiration;
	}
	
	public void setExpiration(long expiration) {
		this.expiration = expiration;
	}
	
	public boolean isAutoRenew() {
		return this.autoRenew;
	}
	
	public void setAutoRenew(boolean autoRenew) {
		this.autoRenew = autoRenew;
	}
}